﻿Arquivo embarc.hpp :
Possui a declaração da classa "embarc" e de suas classes filhas "submarino","portAvioes" e "canoa".
Cada uma das classes filhas foi atribuida com um vetor do tipo inteiro de acordo com quantidade de casas ocupadas pela embarcação,
contendo a quantidade de ataques nescessarios para destruir a parte do barco e tambem suas coordenadas no tabuleiro. 


Arquivo tabuleiro.hpp :
Possui a classe tabuleiro e seus métodos.

Arquivo funçoes.hpp :
Possui todas as funções que não são métodos de nenhuma classe. 
Possui todas as outras funções.

Arquivo main.cpp :
Possui a implementação dos arquivos anteriores formando o jogo Batalha Naval.